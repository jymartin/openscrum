package model;

import java.util.ArrayList;
import java.util.List;

public class UserStory {
    private String name;
    private String description;
    private int estimation;
    private int priority;
    private final List<Resource> resourceList;

    public UserStory(String name, String description, int priority, int estimation) {
        this.resourceList = new ArrayList<>();
        this.name = name;
        this.description = description;
        this.priority = priority;
        this.estimation = estimation;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getEstimation() {
        return estimation;
    }

    public List<Resource> getResources() {
        return resourceList;
    }
    
    public String listToString(){
        String resources = "";
        for (Resource resource : resourceList)
            resources += "[" + resource.getName() + "] ";
        
        return resources;
    } 

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEstimation(int estimation) {
        this.estimation = estimation;
    }
    
    public boolean addResource(Resource resource){
        return resourceList.add(resource);
    }

    @Override
    public String toString() {
        return getName();
    }

    public boolean removeResource(Resource r) {
        return resourceList.remove(r);
    }
    
}
