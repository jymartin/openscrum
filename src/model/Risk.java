package model;

public class Risk {
    private int impact;
    private int prob;
    private String name;

    public Risk(int impact, int prob, String name) {
        this.impact = impact;
        this.prob = prob;
        this.name = name;
    }

    public int getImpact() {
        return impact;
    }

    public void setImpact(int impact) {
        this.impact = impact;
    }

    public int getProb() {
        return prob;
    }

    public void setProb(int prob) {
        this.prob = prob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
