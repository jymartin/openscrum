package model;

import java.util.ArrayList;

public class ProductBacklog {
    private final ArrayList<UserStory> backlog;
    private final ArrayList<Resource> resource;
    private final ArrayList<Resource> resource1;

    public ProductBacklog() {
        backlog = new ArrayList<>();
        resource = new ArrayList<>();
        resource1 = new ArrayList<>();
        resource1.add(new Resource("Mario"));
        
        addResource("Juan");
        addResource("Pablo D");
        addUserStory("DataBase", "Como DV quiero crear una BD para almacenar informacion", 1, 4);
        addUserStory("Crear graficos", "Como DV...", 4, 20);
        addUserStory("Crear sprint", "Como SM quiero...", 1, 10);
    }
    
   public boolean addUserStory(String name, String description, int priority, int estimation){
        return backlog.add(new UserStory( name, description, priority, estimation));
   }
   
   public boolean addResource(String name){
        return resource.add(new Resource(name));
   }
   
   public boolean removeUserStory(UserStory story){
       return backlog.remove(story);
   }

    public ArrayList<UserStory> getUserStories() {
        return backlog;
    }  
    
    public UserStory getLastStory(){
        return backlog.get(backlog.size() - 1);
    }
}
