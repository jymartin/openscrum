package model;

import java.util.ArrayList;

public class Sprint {
    private static int lastId = 0;
    private final ArrayList<UserStory> sprintBacklog;
    private int identifier;

    public Sprint(ArrayList<UserStory> backlog) {
        setIdentifier();
        this.sprintBacklog = backlog;
    }

    public Sprint() {
        setIdentifier();
        sprintBacklog = new ArrayList<>();
    }

    private void setIdentifier() {
        this.identifier = ++lastId;
    }

    public ArrayList<UserStory> getBacklog() {
        return sprintBacklog;
    }
    
    public void addStory(UserStory story){
        sprintBacklog.add(story);
    }
    
    public void removeStory(UserStory story){
        sprintBacklog.remove(story);
    }

    @Override
    public String toString() {
        return "Sprint: " + identifier;
    }
    
    
}
