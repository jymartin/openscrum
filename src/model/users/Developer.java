package model.users;

import model.users.User;

public class Developer extends User{

    public Developer(String userName, String password) {
        super(userName, password, 3);
    }
    
}
