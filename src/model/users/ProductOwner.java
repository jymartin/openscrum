package model.users;

import model.users.User;

public class ProductOwner extends User{

    public ProductOwner(String userName, String password) {
        super(userName, password, 1);
    }
    
}
