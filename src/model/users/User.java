package model.users;

public class User {

    private final String userName;
    private final int role;
    private final String password;
    
    public User(String userName, String password, int role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public int getRole() {
        return role;
    } 
}
