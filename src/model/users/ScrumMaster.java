package model.users;

import model.users.User;

public class ScrumMaster extends User
{

    public ScrumMaster(String userName, String password) {
        super(userName, password, 2);
    }
    
}
