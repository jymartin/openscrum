package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Project {

    private final HashMap<String, Sprint> sprintList;
    private final ProductBacklog productBacklog;
    private final List<Resource> resources;
    private final List<Risk> risks;
    

    public Project() {
        this.sprintList = new HashMap<>();
        this.productBacklog = new ProductBacklog();
        this.resources = new ArrayList<>();
        mockAddResources();
        this.risks = new ArrayList<>();
    }

    public Sprint newSprint() {
        Sprint sprint = new Sprint();
        sprintList.put(sprint.toString(), sprint);
        return sprint;
    }
    
    public Sprint removeSprint(String sprint){
        return sprintList.remove(sprint);
    }

    public ProductBacklog getProductBacklog() {
        return productBacklog;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public List<Risk> getRisks(){
        return risks;
    }
    public boolean addRisk(Risk r){
        return risks.add(r);
    }
    
    public boolean removeRisk(Risk r){
        return risks.remove(r);
    }
    public boolean addResource(Resource r) {
        return resources.add(r);
    }

    public boolean removeResource(Resource r) {
        return resources.remove(r);
    }

    private void mockAddResources() {
        addResource(new Resource("Juan"));
        addResource(new Resource("Mario"));
        addResource(new Resource("Éric"));
        addResource(new Resource("David"));
        addResource(new Resource("Pablo R"));
        addResource(new Resource("Joaquín"));
        addResource(new Resource("Pablo D"));
    }

}
