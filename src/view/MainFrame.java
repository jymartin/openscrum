package view;

import javax.swing.JOptionPane;
import model.Project;
import model.Resource;
import model.users.User;

public class MainFrame extends javax.swing.JFrame {

    private final User user;
    private BacklogPanel backlogPanel;
    private SprintPanel sprintPanel;
    private final Project project;
    private RiskPanel riskPanel;

    MainFrame(User user, Project project) {
        initComponents();
        this.user = user;
        this.project = project;
        initTabbedPane();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        addResourceMenu = new javax.swing.JMenuItem();
        removeResourceMenu = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Open Scrum");
        setIconImage(new javax.swing.ImageIcon("icon.png").getImage());
        setResizable(false);

        jMenu1.setText("Resources");

        addResourceMenu.setText("Add resource");
        addResourceMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addResourceMenuActionPerformed(evt);
            }
        });
        jMenu1.add(addResourceMenu);

        removeResourceMenu.setText("Remove resource");
        removeResourceMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeResourceMenuActionPerformed(evt);
            }
        });
        jMenu1.add(removeResourceMenu);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 710, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 461, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void removeResourceMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeResourceMenuActionPerformed
        Object[] toArray = project.getResources().toArray();
        Resource result = (Resource) JOptionPane.showInputDialog(
                null,
                "Choose a resource",
                "Remove resource",
                JOptionPane.PLAIN_MESSAGE,
                null,
                toArray,
                "Remove resource");
        if(result != null){
            project.removeResource(result);
        }
    }//GEN-LAST:event_removeResourceMenuActionPerformed

    private void addResourceMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addResourceMenuActionPerformed
        String result = (String) JOptionPane.showInputDialog(
                null,
                "Choose a name",
                "Add new resource",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                "New resource");
        if(result != null && !"".equals(result.trim())){
            project.addResource(new Resource(result));
        }
    }//GEN-LAST:event_addResourceMenuActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addResourceMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem removeResourceMenu;
    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables


    private void initTabbedPane() {
        backlogPanel = new BacklogPanel(project, user.getRole());
        backlogPanel.setProductBacklog(project.getProductBacklog());
        sprintPanel = new SprintPanel(user.getRole(), project);
        riskPanel = new RiskPanel(project);
        tabbedPane.add(backlogPanel, "Backlog");
        tabbedPane.add(sprintPanel, "Sprint");
        tabbedPane.add(riskPanel, "Risk");
    }
}
