package view;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.ProductBacklog;
import model.Project;
import model.Resource;
import model.UserStory;

public class BacklogPanel extends javax.swing.JPanel {

    private ProductBacklog productBacklog;
    private Project project;


    public BacklogPanel(Project project, int role) {
        initComponents();

        this.project = project;

        switch (role) {
            case 1:
                userStoryEstimation.setEditable(false);
                userStoryResource.setEditable(false);
                break;
            case 2:
                newUserStoryButton.setEnabled(false);
                break;
            case 3:
                userStoryDescription.setEditable(false);
                userStoryEstimation.setEditable(false);
                userStoryPriority.setEditable(false);
                saveButton.setEnabled(false);
                userStoryResource.setEditable(false);
                newUserStoryButton.setEnabled(false);
        }
    }

    public void setProductBacklog(ProductBacklog productBacklog) {
        this.productBacklog = productBacklog;
        loadUserStoriesFromBacklog();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        userStoryResource = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        userStoriesList = new javax.swing.JList<UserStory>();
        newUserStoryButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        userStoryTitle = new javax.swing.JTextField();
        resourceLabel = new javax.swing.JLabel();
        priorityLabel = new javax.swing.JLabel();
        userStoryPriority = new javax.swing.JTextField();
        estimationLabel = new javax.swing.JLabel();
        userStoryEstimation = new javax.swing.JTextField();
        descriptionLabel = new javax.swing.JLabel();
        userStoryDescriptionScroll = new javax.swing.JScrollPane();
        userStoryDescription = new javax.swing.JTextArea();
        editResourcesButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        resourceList = new javax.swing.JList<Resource>();

        setPreferredSize(new java.awt.Dimension(770, 700));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        userStoriesList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                userStoriesListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(userStoriesList);

        newUserStoryButton.setText("Add User Story");
        newUserStoryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newUserStoryButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(newUserStoryButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(newUserStoryButton)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        titleLabel.setText("Title");

        userStoryTitle.setToolTipText("Título");

        resourceLabel.setText("Resources");

        priorityLabel.setText("Priority");

        userStoryPriority.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userStoryPriorityActionPerformed(evt);
            }
        });

        estimationLabel.setText("Estimation");

        descriptionLabel.setText("Description");

        userStoryDescription.setColumns(20);
        userStoryDescription.setLineWrap(true);
        userStoryDescription.setRows(5);
        userStoryDescription.setWrapStyleWord(true);
        userStoryDescriptionScroll.setViewportView(userStoryDescription);

        editResourcesButton.setText("Edit Resources");
        editResourcesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editResourcesButtonActionPerformed(evt);
            }
        });

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        resourceList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(resourceList);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(titleLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(descriptionLabel)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(userStoryDescriptionScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(resourceLabel)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(userStoryPriority, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(priorityLabel))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(estimationLabel)
                                            .addComponent(userStoryEstimation, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(userStoryTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(editResourcesButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(saveButton)))
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(userStoryTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(priorityLabel)
                    .addComponent(estimationLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userStoryPriority, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userStoryEstimation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descriptionLabel)
                    .addComponent(resourceLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(userStoryDescriptionScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editResourcesButton)
                    .addComponent(saveButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void userStoryPriorityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userStoryPriorityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userStoryPriorityActionPerformed

    private void userStoriesListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_userStoriesListValueChanged
        updateUserStoryFields(getSelectedStory());
    }//GEN-LAST:event_userStoriesListValueChanged

    private void newUserStoryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newUserStoryButtonActionPerformed
        productBacklog.addUserStory("New story", "", 0, 0);
        DefaultListModel model = (DefaultListModel) userStoriesList.getModel();
        model.addElement(productBacklog.getLastStory());
    }//GEN-LAST:event_newUserStoryButtonActionPerformed

    private void editResourcesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editResourcesButtonActionPerformed
        if (userStoriesList.getSelectedIndex() != -1) {
            new SelectResources(project, userStoriesList.getSelectedValue()).setVisible(true);
        }
    }//GEN-LAST:event_editResourcesButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        if (getSelectedStory() == null) {
            JOptionPane.showMessageDialog(new JFrame(), "Please, select an existing user story");
            return;
        }
        try {
            int priority = Integer.parseInt(userStoryPriority.getText());
            UserStory userStory = getSelectedStory();
            userStory.setName(userStoryTitle.getText());
            userStory.setDescription(userStoryDescription.getText());
            userStory.setPriority(priority);
            userStory.setEstimation(Integer.parseInt(userStoryEstimation.getText()));
        } catch (Exception e) {
            return;
        }
        userStoriesList.updateUI();
    }//GEN-LAST:event_saveButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JButton editResourcesButton;
    private javax.swing.JLabel estimationLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton newUserStoryButton;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JLabel resourceLabel;
    private javax.swing.JList<Resource> resourceList;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JList<UserStory> userStoriesList;
    private javax.swing.JTextArea userStoryDescription;
    private javax.swing.JScrollPane userStoryDescriptionScroll;
    private javax.swing.JTextField userStoryEstimation;
    private javax.swing.JTextField userStoryPriority;
    private javax.swing.JTextField userStoryResource;
    private javax.swing.JTextField userStoryTitle;
    // End of variables declaration//GEN-END:variables

    private void loadUserStoriesFromBacklog() {
        DefaultListModel listModel = new DefaultListModel();
        productBacklog.getUserStories().forEach((userStory) -> {
            listModel.addElement(userStory);
        });
        userStoriesList.setModel(listModel);
    }

    private void updateUserStoryFields(UserStory userStory) {
        userStoryTitle.setText(userStory.getName());
        userStoryDescription.setText(userStory.getDescription());
        userStoryPriority.setText(String.valueOf(userStory.getPriority()));
        userStoryEstimation.setText(String.valueOf(userStory.getEstimation()));
        userStoryResource.setText(userStory.listToString());
        DefaultListModel<Resource> listModel = new DefaultListModel<>();
        for (Resource resource : userStory.getResources()) {
            listModel.addElement(resource);
        }
        resourceList.setModel(listModel);
    }

    private UserStory getSelectedStory() {
        return (UserStory) userStoriesList.getSelectedValue();
    }
}
