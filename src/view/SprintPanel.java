/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import model.Project;
import model.Sprint;
import model.UserStory;

/**
 *
 * @author joaqu
 */
public class SprintPanel extends javax.swing.JPanel {

    /**
     * Creates new form BacklogPanel
     */
    private Project project;

    public SprintPanel() {
        initComponents();
    }

    SprintPanel(int role, Project product) {
        initComponents();
        this.project = product;
        /*switch (role) {
         case 1:
         userStoryEstimation.setEditable(false);
         break;
         case 2:
         break;
         case 3:
         userStoryDescription.setEditable(false);
         userStoryEstimation.setEditable(false);
         userStoryPriority.setEditable(false);
         userStoryTitle.setEditable(false);
         }*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        sprintLabel = new javax.swing.JLabel();
        sprintCombo = new javax.swing.JComboBox<Sprint>();
        jScrollPane2 = new javax.swing.JScrollPane();
        userStoriesList = new javax.swing.JList();
        newSprintButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        userStoryTitle = new javax.swing.JTextField();
        descriptionLabel = new javax.swing.JLabel();
        userStoryDescriptionScroll = new javax.swing.JScrollPane();
        userStoryDescription = new javax.swing.JTextArea();
        priorityLabel = new javax.swing.JLabel();
        userStoryPriority = new javax.swing.JTextField();
        userStoryEstimation = new javax.swing.JTextField();
        estimationLabel = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        totalEstimationLabel = new javax.swing.JLabel();
        totalEstimation = new javax.swing.JTextField();
        deleteSprintButton = new javax.swing.JButton();
        editSprintButton = new javax.swing.JButton();

        setToolTipText("");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        sprintLabel.setText("Sprint:");

        sprintCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sprintComboActionPerformed(evt);
            }
        });

        userStoriesList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                userStoriesListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(userStoriesList);

        newSprintButton.setText("New Sprint");
        newSprintButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newSprintButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(sprintLabel)
                        .addGap(18, 18, 18)
                        .addComponent(sprintCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(newSprintButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sprintLabel)
                    .addComponent(sprintCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newSprintButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        titleLabel.setText("Title");

        userStoryTitle.setEditable(false);
        userStoryTitle.setToolTipText("Título");

        descriptionLabel.setText("Description");

        userStoryDescription.setEditable(false);
        userStoryDescription.setColumns(20);
        userStoryDescription.setLineWrap(true);
        userStoryDescription.setRows(5);
        userStoryDescription.setWrapStyleWord(true);
        userStoryDescriptionScroll.setViewportView(userStoryDescription);

        priorityLabel.setText("Priority");

        userStoryPriority.setEditable(false);

        userStoryEstimation.setEditable(false);

        estimationLabel.setText("Estimation");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(titleLabel)
                            .addComponent(userStoryTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(descriptionLabel)
                                    .addComponent(priorityLabel)
                                    .addComponent(userStoryPriority, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(userStoryEstimation, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(estimationLabel))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(userStoryDescriptionScroll)
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(userStoryTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(descriptionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(userStoryDescriptionScroll, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(priorityLabel)
                    .addComponent(estimationLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userStoryPriority, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userStoryEstimation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        totalEstimationLabel.setText("Total estimation");

        totalEstimation.setEditable(false);

        deleteSprintButton.setText("Delete Sprint");
        deleteSprintButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSprintButtonActionPerformed(evt);
            }
        });

        editSprintButton.setText("Edit Sprint");
        editSprintButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editSprintButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totalEstimationLabel)
                    .addComponent(totalEstimation, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(150, Short.MAX_VALUE)
                .addComponent(editSprintButton)
                .addGap(18, 18, 18)
                .addComponent(deleteSprintButton)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(totalEstimationLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalEstimation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteSprintButton)
                    .addComponent(editSprintButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void userStoriesListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_userStoriesListValueChanged
        if (getSelectedStory() != null) {
            updateUserStoryFields(getSelectedStory());
        }
    }//GEN-LAST:event_userStoriesListValueChanged

    private void sprintComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sprintComboActionPerformed
        if (sprintCombo.getSelectedIndex() != -1) {
            refreshSprintBacklogList();
            setTotalEstimated();
        }
    }//GEN-LAST:event_sprintComboActionPerformed

    private void newSprintButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newSprintButtonActionPerformed
        newSprint();
    }//GEN-LAST:event_newSprintButtonActionPerformed

    private void deleteSprintButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSprintButtonActionPerformed
        if (sprintCombo.getSelectedIndex() != -1) {
            Sprint sprint = (Sprint) sprintCombo.getSelectedItem();
            sprintCombo.removeItem(sprint);
            project.removeSprint(sprint.toString());
            emptyFields();
            //sprintCombo.setSelectedIndex(0);
            if(sprintCombo.getItemCount() == 0){
                userStoriesList.setModel(new DefaultListModel());
            }
        }
    }//GEN-LAST:event_deleteSprintButtonActionPerformed

    private void editSprintButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSprintButtonActionPerformed
        if (sprintCombo.getSelectedIndex() != -1) {
            new SelectStories(project, (Sprint) sprintCombo.getSelectedItem()).setVisible(true);
        }
    }//GEN-LAST:event_editSprintButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton deleteSprintButton;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JButton editSprintButton;
    private javax.swing.JLabel estimationLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton newSprintButton;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JComboBox<Sprint> sprintCombo;
    private javax.swing.JLabel sprintLabel;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JTextField totalEstimation;
    private javax.swing.JLabel totalEstimationLabel;
    private javax.swing.JList userStoriesList;
    private javax.swing.JTextArea userStoryDescription;
    private javax.swing.JScrollPane userStoryDescriptionScroll;
    private javax.swing.JTextField userStoryEstimation;
    private javax.swing.JTextField userStoryPriority;
    private javax.swing.JTextField userStoryTitle;
    // End of variables declaration//GEN-END:variables

    private void updateUserStoryFields(UserStory userStory) {
        userStoryTitle.setText(userStory.getName());
        userStoryDescription.setText(userStory.getDescription());
        userStoryPriority.setText(String.valueOf(userStory.getPriority()));
        userStoryEstimation.setText(String.valueOf(userStory.getEstimation()));
        setTotalEstimated();
    }

    private UserStory getSelectedStory() {
        return (UserStory) userStoriesList.getSelectedValue();
    }

    void newSprint() {
        Sprint sprint = project.newSprint();
        sprintCombo.addItem(sprint);
        refreshSprintBacklogList();
    }

    private void refreshSprintBacklogList() {
        if (sprintCombo.getSelectedItem() != null) {
            Sprint selectedSprint = (Sprint) sprintCombo.getSelectedItem();
            DefaultListModel listModel = new DefaultListModel();
            selectedSprint.getBacklog().forEach((userStory) -> {
                listModel.addElement(userStory);
            });
            userStoriesList.setModel(listModel);
        }
    }

    private void emptyFields() {
        userStoryTitle.setText("");
        userStoryDescription.setText("");
        userStoryPriority.setText("");
        userStoryEstimation.setText("");
        totalEstimation.setText("");
    }

    private void setTotalEstimated() {
        int total = 0;
        for (UserStory story : ((Sprint) sprintCombo.getSelectedItem()).getBacklog()) {
            total += story.getEstimation();
        }
        totalEstimation.setText(total + "");
    }

}
